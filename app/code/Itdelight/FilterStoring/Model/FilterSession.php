<?php
/**
 * @author IT Delight
 * @copyright Copyright (c) 2018 Convert (http://itdelight.com/)
 */
namespace Itdelight\FilterStoring\Model;

use Magento\Framework\DataObject;
use Magento\Catalog\Model\Session;

/**
 * Class FilterSession
 *
 * @package Itdelight\FilterStoring\Model
 */
class FilterSession extends DataObject
{
    /**
     * Storing filter
     */
    const ATTRIBUTE = 'size';

    /**
     * @var Session
     */
    protected $catalogSession;

    /**
     * @var array
     */
    protected $size = [];

    /**
     * FilterSession constructor.
     *
     * @param Session $catalogSession
     * @param array $data
     */
    public function __construct(
        Session $catalogSession,
        array $data = []
    ) {
        parent::__construct($data);
        $this->catalogSession = $catalogSession;
    }

    /**
     * Get storing category filter
     *
     * @param null $categoryId
     * @return bool|mixed
     */
    public function getSize($categoryId = null)
    {
        if ($categoryId == null) {
            $categoryId = $this->catalogSession->getData('current_category');
        }
        if (empty($this->size)) {
            $this->size = $this->catalogSession->getData(self::ATTRIBUTE);
        }
        if (isset($this->size[$categoryId])) {
            return $this->size[$categoryId];
        }

        return false;
    }

    /**
     * Set filter value
     *
     * @param int|string $size
     * @param int $categoryId
     */
    public function setSize($size, $categoryId)
    {
        $filterStoring = $this->catalogSession->getData();
        $filterStoring[self::ATTRIBUTE][$categoryId] = $size;
        $this->catalogSession->setData($filterStoring);
        $this->size[$categoryId] = $size;
    }

    /**
     * Set current category
     *
     * @param int $categoryId
     */
    public function setCategoryName($categoryId)
    {
        $this->catalogSession->setData('current_category', $categoryId);
    }

    /**
     * Unset storing category's filter
     *
     * @param $categoryId
     */
    public function unsetStoringFilter($categoryId)
    {
        if (empty($this->size)) {
            $this->size = $this->catalogSession->getData(self::ATTRIBUTE);
        }

        unset($categoryId, $this->size);
    }
}
