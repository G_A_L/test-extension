<?php
/**
 * @author IT Delight
 * @copyright Copyright (c) 2018 Convert (http://itdelight.com/)
 */
namespace Itdelight\FilterStoring\Plugin\Model\Layer\Filter;

use Magento\Catalog\Model\Layer\Filter\FilterInterface;
use Magento\Framework\App\RequestInterface;
use Itdelight\FilterStoring\Model\FilterSession;
use Magento\Catalog\Model\ResourceModel\Layer\Filter\Attribute;
use Itdelight\FilterStoring\Helper\Data;

/**
 * Class FilterInterfacePlugin
 *
 * @package Itdelight\FilterStoring\Plugin\Model\Layer\Filter
 */
class FilterInterfacePlugin
{
    /**
     * @var FilterSession
     */
    protected $filterSession;

    /**
     * @var Attribute
     */
    protected $attribute;

    /**
     * @var Data
     */
    protected $filterHelper;

    /**
     * FilterInterfacePlugin constructor.
     *
     * @param FilterSession $filterSession
     * @param Attribute $attribute
     * @param Data $filterHelper
     */
    public function __construct(
        FilterSession $filterSession,
        Attribute $attribute,
        Data $filterHelper
    ) {
        $this->filterSession = $filterSession;
        $this->attribute = $attribute;
        $this->filterHelper = $filterHelper;
    }

    /**
     * Set storing filter befor apply filters
     *
     * @param FilterInterface $subject
     * @param RequestInterface $request
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function beforeApply(
        FilterInterface $subject,
        RequestInterface $request
    ) {
        if ($this->filterHelper->isEnableFilterStoring()) {
            $attributeCode = $subject->getRequestVar();

            if (FilterSession::ATTRIBUTE != $attributeCode && $request->getRouteName() == 'catalog') {
                return [$request];
            }

            $size = $this->filterSession->getSize($request->getParam('id', null));
            if ($size && $this->isApplicableFilter($subject, $size)) {
                $request->setParams([FilterSession::ATTRIBUTE => $size]);
            }
        }

        return [$request];
    }

    /**
     * Check possibility to apply filter
     *
     * @param $filter
     * @param $value
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function isApplicableFilter($filter, $value)
    {
        $count = $this->attribute->getCount($filter);

        if (isset($count[$value]) && (int)$count[$value] > 0) {
            return true;
        }

        return false;
    }
}
