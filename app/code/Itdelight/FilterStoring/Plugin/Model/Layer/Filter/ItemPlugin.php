<?php
/**
 * @author IT Delight
 * @copyright Copyright (c) 2018 Convert (http://itdelight.com/)
 */
namespace Itdelight\FilterStoring\Plugin\Model\Layer\Filter;

use Itdelight\FilterStoring\Model\FilterSession;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Catalog\Model\Layer\Filter\Item;

/**
 * Class ItemPlugin
 *
 * @package Itdelight\FilterStoring\Plugin\Model\Layer\Filter
 */
class ItemPlugin
{
    /**
     * @var FilterSession
     */
    protected $filterSession;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * ItemPlugin constructor.
     *
     * @param FilterSession $filterSession
     * @param Registry $registry
     * @param UrlInterface $urlBuilder
     */
    public function __construct(
        FilterSession $filterSession,
        Registry $registry,
        UrlInterface $urlBuilder
    ) {
        $this->filterSession = $filterSession;
        $this->registry = $registry;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * Add storing attribute removing
     *
     * @param Item $subject
     * @param $result
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function afterGetRemoveUrl(
        Item $subject,
        $result
    ) {
        $query = [$subject->getFilter()->getRequestVar() => $subject->getFilter()->getResetValue()];
        $categoryId = $this->registry->registry('current_category')->getId();
        if ($subject->getFilter()->getRequestVar() == FilterSession::ATTRIBUTE
            && $this->filterSession->getSize($categoryId)
        ) {
            $query['clean'] = $categoryId;
        } else {
            $query['clean'] = null;
        }
        $params['_current'] = true;
        $params['_use_rewrite'] = true;
        $params['_query'] = $query;
        $params['_escape'] = true;

        return $this->urlBuilder->getUrl('*/*/*', $params);
    }
}
