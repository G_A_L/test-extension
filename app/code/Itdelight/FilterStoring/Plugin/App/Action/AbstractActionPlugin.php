<?php
/**
 * @author IT Delight
 * @copyright Copyright (c) 2018 Convert (http://itdelight.com/)
 */
namespace Itdelight\FilterStoring\Plugin\App\Action;

use Itdelight\FilterStoring\Model\FilterSession;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Framework\App\Action\AbstractAction;
use Magento\Framework\App\RequestInterface;
use Itdelight\FilterStoring\Helper\Data;

/**
 * Class AbstractActionPlugin
 *
 * @package Itdelight\FilterStoring\Plugin\App\Action
 */
class AbstractActionPlugin
{
    /**
     * @var FilterSession
     */
    protected $filterSession;

    /**
     * @var HttpContext
     */
    protected $httpContext;

    /**
     * @var Data
     */
    protected $filterHelper;

    /**
     * AbstractActionPlugin constructor.
     *
     * @param HttpContext $httpContext
     * @param FilterSession $filterSession
     * @param Data $filterHelper
     */
    public function __construct(
        HttpContext $httpContext,
        FilterSession $filterSession,
        Data $filterHelper
    ) {
        $this->httpContext = $httpContext;
        $this->filterSession = $filterSession;
        $this->filterHelper = $filterHelper;
    }

    /**
     * Add storing filter to context
     *
     * @param AbstractAction $subject
     * @param RequestInterface $request
     * @return array
     */
    public function beforeDispatch(
        AbstractAction $subject,
        RequestInterface $request
    ) {
        if ($this->filterHelper->isEnableFilterStoring()) {
            $filterValue = $this->filterSession->getSize();
            if ($filterValue) {
                $this->httpContext->setValue(FilterSession::ATTRIBUTE, $filterValue, false);
            }
        }

        return [$request];
    }
}
