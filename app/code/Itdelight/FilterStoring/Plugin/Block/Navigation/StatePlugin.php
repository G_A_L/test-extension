<?php
/**
 * @author IT Delight
 * @copyright Copyright (c) 2018 Convert (http://itdelight.com/)
 */
namespace Itdelight\FilterStoring\Plugin\Block\Navigation;

use Itdelight\FilterStoring\Model\FilterSession;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\LayeredNavigation\Block\Navigation\State;

/**
 * Class StatePlugin
 *
 * @package Itdelight\FilterStoring\Plugin\Block\Navigation
 */
class StatePlugin
{
    /**
     * @var FilterSession
     */
    protected $filterSession;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * StatePlugin constructor.
     *
     * @param FilterSession $filterSession
     * @param Registry $registry
     * @param UrlInterface $urlBuilder
     */
    public function __construct(
        FilterSession $filterSession,
        Registry $registry,
        UrlInterface $urlBuilder
    ) {
        $this->filterSession = $filterSession;
        $this->registry = $registry;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * Add removing storing filter with all filter.
     *
     * @param State $subject
     * @param $result
     * @return string
     */
    public function afterGetClearUrl(
        State $subject,
        $result
    ) {
        if (is_array($subject->getActiveFilters())) {
            $filterState = [];
            foreach ($subject->getActiveFilters() as $filter) {
                $categoryId = $this->registry->registry('current_category')->getId();
                if ($filter->getFilter()->getRequestVar() == FilterSession::ATTRIBUTE
                    && $this->filterSession->getSize($categoryId)
                ) {
                    $filterState['clean'] = $categoryId;
                } else {
                    $filterState['clean'] = null;
                }

                $filterState[$filter->getFilter()->getRequestVar()] = $filter->getFilter()->getCleanValue();
            }

            $params['_current'] = true;
            $params['_use_rewrite'] = true;
            $params['_query'] = $filterState;
            $params['_escape'] = true;

            return $this->urlBuilder->getUrl('*/*/*', $params);
        }

        return $result;
    }
}
