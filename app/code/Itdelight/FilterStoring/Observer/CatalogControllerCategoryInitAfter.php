<?php
/**
 * @author IT Delight
 * @copyright Copyright (c) 2018 Convert (http://itdelight.com/)
 */
namespace Itdelight\FilterStoring\Observer;

use Itdelight\FilterStoring\Model\FilterSession;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Itdelight\FilterStoring\Helper\Data;

/**
 * Class CatalogControllerCategoryInitAfter
 *
 * @package Itdelight\FilterStoring\Observer
 */
class CatalogControllerCategoryInitAfter implements ObserverInterface
{
    /**
     * @var FilterSession
     */
    protected $filterSession;

    /**
     * @var Data
     */
    protected $filterHelper;

    /**
     * CatalogControllerCategoryInitAfter constructor.
     *
     * @param FilterSession $filterSession
     * @param Data $filterHelper
     */
    public function __construct(
        FilterSession $filterSession,
        Data $filterHelper
    ) {
        $this->filterSession = $filterSession;
        $this->filterHelper = $filterHelper;
    }

    /**
     * Set parameters for storing filter
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        if ($this->filterHelper->isEnableFilterStoring()) {
            $action = $observer->getEvent()->getControllerAction();
            $categoryId = $observer->getEvent()->getCategory()->getId();
            $request = $action->getRequest();
            $cleanCategoryId = $request->getParam('clean');
            $request->setParam('clean', false);
            $size = $request->getParam(FilterSession::ATTRIBUTE);
            $this->filterSession->setCategoryName($categoryId);


            if ($size && $categoryId != $cleanCategoryId) {
                $this->filterSession->setSize($size, $categoryId);
            }

            if ($cleanCategoryId) {
                $this->filterSession->unsetStoringFilter($cleanCategoryId);
            }
        }
    }
}
