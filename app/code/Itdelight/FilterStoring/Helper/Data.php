<?php
/**
 * @author IT Delight
 * @copyright Copyright (c) 2018 Convert (http://itdelight.com/)
 */
namespace Itdelight\FilterStoring\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Data
 *
 * @package Itdelight\FilterStoring\Helper
 */
class Data extends AbstractHelper
{
    /**
     * Storing filter configuration
     */
    const  STORING_FILTER_STATUS = 'storing_filter/filter_configuration/is_enable';

    /**
     * Get storing filter status
     *
     * @return bool
     */
    public function isEnableFilterStoring()
    {
        return $this->scopeConfig->isSetFlag(
            self::STORING_FILTER_STATUS,
            ScopeInterface::SCOPE_STORE
        );
    }
}
