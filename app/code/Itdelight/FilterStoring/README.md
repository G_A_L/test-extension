#IT Delight Filter Storing

Storing selected size attribute by categories
- you can disable module functionality in the admin panel;
- storing filter will be clearing with Magento layered navigation 
functionality for removing and clearing selected filters;

## Dependencies
The **Itdelight_FilterStoring** is dependent on following modules:

- Magento_Catalog;
- Magento_LayeredNavigation;
