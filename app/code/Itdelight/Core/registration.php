<?php
/**
 * @author IT Delight
 * @copyright Copyright (c) 2019 Convert (http://itdelight.com/)
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Itdelight_Core',
    __DIR__
);
