<?php
/**
 * @author IT Delight
 * @copyright Copyright (c) 2018 Convert (http://itdelight.com/)
 */

namespace Itdelight\CustomUrl\Controller\Itdelight;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class ItdelightFront
 *
 * @package Itdelight\CustomUrl\Controller\Itdelight
 */
class ItdelightFront extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * ItdelightFront constructor.
     *
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $result = $this->resultPageFactory->create();
        return $result;
    }
}
