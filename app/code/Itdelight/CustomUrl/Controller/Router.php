<?php

namespace Itdelight\CustomUrl\Controller;

class Router implements \Magento\Framework\App\RouterInterface
{
    /**
     * @var \Magento\Framework\App\ActionFactory
     */
    protected $actionFactory;
    /**
     * Response
     *
     * @var \Magento\Framework\App\ResponseInterface
     */
    protected $response;
    /**
     * @param \Magento\Framework\App\ActionFactory $actionFactory
     * @param \Magento\Framework\App\ResponseInterface $response
     */
    public function __construct(
        \Magento\Framework\App\ActionFactory $actionFactory,
        \Magento\Framework\App\ResponseInterface $response
    ) {
        $this->actionFactory = $actionFactory;
        $this->response = $response;
    }
    /**
     * Validate and Match
     *
     * @param \Magento\Framework\App\RequestInterface $request
     * @return bool
     */
    public function match(\Magento\Framework\App\RequestInterface $request)
    {
        $identifier = trim($request->getPathInfo(), '/');
        $urlParams = explode('/', $identifier);
        if (count($urlParams) == 2
            && strpos($urlParams[0], 'itdelightFront')
            !== false
            && strpos($urlParams[1], 'short') !== false) {
            /*
             * We must set module, controller path and action name + we will set page id 5 witch is about us page on
             * default magento 2 installation with sample data.
             */
//            $request->setModuleName('cms')->setControllerName('page')->setActionName('view')->setParam('page_id', 4);
            $request->setModuleName('itdelight')
                    ->setControllerName('itdelight')
                    ->setActionName('itdelightFront');

//            return $this->actionFactory->create(\Magento\Framework\App\Action\Forward::class);
        }
        return false;
    }
}
