<?php

namespace Itdelight\CustomerSection\CustomerData;

use Magento\Customer\CustomerData\SectionSourceInterface;

class FreeShipping extends \Magento\Framework\DataObject implements SectionSourceInterface
{
    protected $checkoutSession;

    /**
     * @var \Magento\Checkout\Helper\Data
     */
    protected $checkoutHelper;

    protected $freeShippingAmount = 40;

    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Checkout\Helper\Data $checkoutHelper,
        array $data = []
    ) {
        parent::__construct($data);
        $this->checkoutSession = $checkoutSession;
        $this->checkoutHelper = $checkoutHelper;
    }

    public function getSectionData()
    {
        $subtotalAmount = $this->checkoutSession->getQuote()->getSubtotal();
        return [
            'hasFreeShipping' => (int)$subtotalAmount >= (int)$this->freeShippingAmount,
            'amountLeft' => $this->checkoutHelper->formatPrice((int)$this->freeShippingAmount - (int)$subtotalAmount),
        ];
    }
}
