<?php

namespace Itdelight\CustomerSection\ViewModel;

class FreeShipping implements \Magento\Framework\View\Element\Block\ArgumentInterface
{
    protected $coreRegistry;

    protected $checkoutSession;

    protected $checkoutHelper;

    protected $freeShippingAmount = 40;

    public function __construct(
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Checkout\Helper\Data $checkoutHelper
    ) {
        $this->coreRegistry = $coreRegistry;
        $this->checkoutSession = $checkoutSession;
        $this->checkoutHelper = $checkoutHelper;
    }

    public function canShipForFree()
    {
        $freeShippingAmount = 20;
        $productPrice = 0;
        $product = $this->coreRegistry->registry('current_products');
        if ($product) {
            $productPrice = $product->getFinalPrice();
        }

        return $product >= $freeShippingAmount;
    }

    public function freeShippingAvailable()
    {
        $subtotalAmount = $this->checkoutSession->getQuote()->getSubtotal();
        return (int)$subtotalAmount >= (int)$this->freeShippingAmount;
    }

    public function amountLeft()
    {
        $subtotalAmount = $this->checkoutSession->getQuote()->getSubtotal();

        return $this->checkoutHelper->formatPrice((int)$this->freeShippingAmount - (int)$subtotalAmount);
    }
}
