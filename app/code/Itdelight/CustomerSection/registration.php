<?php
/**
 * @author IT Delight
 * @copyright Copyright (c) 2020 Convert (http://itdelight.com/)
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Itdelight_CustomerSection',
    __DIR__
);
