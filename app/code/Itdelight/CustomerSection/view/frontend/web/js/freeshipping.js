define([
    'uiComponent',
    'Magento_Customer/js/customer-data'
], function (Component, customerData) {
    'use strict';
    return Component.extend({
        defaults: {
            template: 'Itdelight_CustomerSection/freeshipping'
        },

        /** @inheritdoc */
        initialize: function () {
            this._super();
            this.freeShippingData = customerData.get('freeshipping');
            return this;
        },
    });
});
