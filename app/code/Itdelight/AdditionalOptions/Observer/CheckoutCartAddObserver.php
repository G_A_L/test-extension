<?php

namespace Itdelight\AdditionalOptions\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Serialize\Serializer\Json;

/**
 * Class CheckoutCartAddObserver
 * @package Itdelight\AdditionalOptions\Observer
 */
class CheckoutCartAddObserver implements ObserverInterface
{
    /**
     * @var Json
     */
    protected $serialize;

    /**
     * CheckoutCartAddObserver constructor.
     * @param Json $serialize
     */
    public function __construct(
        Json $serialize
    ) {
        $this->serialize = $serialize;
    }

    /**
     * @param EventObserver $observer
     */
    public function execute(EventObserver $observer)
    {
        $item = $observer->getQuoteItem();
        $additionalOptions = [];

        if ($additionalOption = $item->getOptionByCode('additional_options')) {
            $additionalOptions = (array) $this->serialize->unserialize($additionalOption->getValue());
        }

        $additionalOptions[] = [
            'label' => 'Additional Options Label',
            'value' => 'Additional Options Value Observer'
        ];

        if (count($additionalOptions) > 0) {
            $item->addOption(
                [
                    'code' => 'additional_options',
                    'value' => $this->serialize->serialize($additionalOptions)
                ]
            );
        }
    }
}
