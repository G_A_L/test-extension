<?php
/**
 * @author IT Delight
 * @copyright Copyright (c) 2018 Convert (http://itdelight.com/)
 */

namespace Itdelight\AdditionalOptions\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Serialize\Serializer\Json;

/**
 * Class QuoteSubmitObserver
 * @package Itdelight\AdditionalOptions\Observer
 */
class QuoteSubmitObserver implements ObserverInterface
{
    /**
     * @var Json
     */
    protected $serialize;
    /**
     * @var array
     */
    protected $quoteItems = [];
    /**
     * @var null
     */
    protected $quote = null;
    /**
     * @var null
     */
    protected $order = null;

    /**
     * CheckoutCartAddObserver constructor.
     * @param Json $serialize
     */
    public function __construct(
        Json $serialize
    ) {
        $this->serialize = $serialize;
    }

    /**
     * @param EventObserver $observer
     */
    public function execute(EventObserver $observer)
    {
        $this->quote = $observer->getQuote();
        $this->order = $observer->getOrder();

        foreach ($this->order->getItems() as $orderItem) {
            if ($quoteItem = $this->getQuoteItemById($orderItem->getQuoteItemId())) {
                if ($additionalOptionsQuote = $quoteItem->getOptionByCode('additional_options')) {
                    if ($additionalOptionsOrder = $orderItem->getProductOptionByCode('additional_options')) {
                        $additionalOptions = array_merge($additionalOptionsQuote, $additionalOptionsOrder);
                    } else {
                        $additionalOptions = $additionalOptionsQuote;
                    }
                    if (count($additionalOptions) > 0) {
                        $options = $orderItem->getProductOptions();
                        $options['additional_options'] = $this->serialize->unserialize($additionalOptions->getValue());
                        $orderItem->setProductOptions($options);
                    }
                }
            }
        }
    }

    /**
     * @param $id
     * @return mixed|null
     */
    private function getQuoteItemById($id)
    {
        if (empty($this->quoteItems)) {
            foreach ($this->quote->getItems() as $item) {
                $this->quoteItems[$item->getId()] = $item;
            }
        }
        if (array_key_exists($id, $this->quoteItems)) {
            return $this->quoteItems[$id];
        }
        return null;
    }
}
