<?php

namespace Itdelight\AdditionalOptions\Plugin\Model\Carrier;

use Magento\Shipping\Model\Carrier\AbstractCarrierOnline;

/**
 * Class AbstractCarrierOnlinePlugin
 *
 * @package Itdelight\AdditionalOptions\Plugin\Model\Carrier
 */
class AbstractCarrierOnlinePlugin
{
    /**
     * @param AbstractCarrierOnline $subject
     * @param $result
     * @return mixed
     */
    public function afterGetTrackingInfo(
        AbstractCarrierOnline $subject,
        $result
    ) {
        return $result;
    }
}
