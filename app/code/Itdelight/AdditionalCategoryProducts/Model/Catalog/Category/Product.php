<?php
namespace Itdelight\AdditionalCategoryProducts\Model\Catalog\Category;

/**
 * Class Product
 *
 * @package Itdelight\AdditionalCategoryProducts\Model\Catalog\Category
 */
class Product extends \Magento\Framework\Model\AbstractModel implements
    \Itdelight\AdditionalCategoryProducts\Api\Data\ProductInterface
{
    /**
     * Default type of additional category product
     */
    const TYPE_DEFAULT = 0;

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(
            \Itdelight\AdditionalCategoryProducts\Model\ResourceModel\Catalog\Category\Product::class
        );
    }

    /**
     * @return mixed
     */
    public function getCategoryProductId()
    {
        return $this->getData(self::CATEGORY_PRODUCT_ID);
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->getData(self::CATEGORY_ID);
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->getData(self::PRODUCT_ID);
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->getData(self::POSITION);
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->getData(self::TYPE);
    }

    /**
     * @param $categoryProductId
     * @return Product
     */
    public function setCategoryProductId($categoryProductId)
    {
        return $this->setData(self::CATEGORY_PRODUCT_ID, $categoryProductId);
    }

    /**
     * @param $categoryId
     * @return Product
     */
    public function setCategoryId($categoryId)
    {
        return $this->setData(self::CATEGORY_ID, $categoryId);
    }

    /**
     * @param $productId
     * @return Product
     */
    public function setProductId($productId)
    {
        return $this->setData(self::PRODUCT_ID, $productId);
    }

    /**
     * @param $position
     * @return Product
     */
    public function setPosition($position)
    {
        return $this->setData(self::POSITION, $position);
    }

    /**
     * @param $description
     * @return Product
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * @param $type
     * @return Product
     */
    public function setType($type)
    {
        return $this->setData(self::TYPE, $type);
    }
}
