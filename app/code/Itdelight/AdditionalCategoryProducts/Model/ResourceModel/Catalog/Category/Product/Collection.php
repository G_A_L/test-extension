<?php
namespace Itdelight\AdditionalCategoryProducts\Model\ResourceModel\Catalog\Category\Product;

use Itdelight\AdditionalCategoryProducts\Model\ResourceModel\Catalog\Category\Product;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * Additional category products collection
 */
class Collection extends AbstractCollection
{
    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init(
            \Itdelight\AdditionalCategoryProducts\Model\Catalog\Category\Product::class,
            Product::class
        );
    }
}
