<?php
namespace Itdelight\AdditionalCategoryProducts\Model\ResourceModel\Catalog\Category;

/**
 * Class Product
 * Additional category product resource model
 */
class Product extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Table name
     */
    const TABLE = 'itdelight_category_additional_products';

    /**
     * Name of primary key
     */
    const PRIMARY_KEY = 'category_product_id';

    /**
     * {@inheritdoc}
     */
    public function _construct()
    {
        $this->_init(static::TABLE, static::PRIMARY_KEY);
    }
}
