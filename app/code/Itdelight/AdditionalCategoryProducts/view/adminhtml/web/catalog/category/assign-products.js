/* global $H */
define([
    'jquery',
    'mage/adminhtml/grid'
], function ($) {
    'use strict';

    return function (config) {
        var selectedProducts = config.selectedProducts,
            categoryProducts = $H(selectedProducts),
            gridJsObject = window[config.gridJsObjectName],
            $categoryProducts = $('#itdelight_in_category_products');

        $categoryProducts.val(Object.toJSON(categoryProducts));

        /**
         * Register Category Product
         *
         * @param {Object} grid
         * @param {Object} element
         * @param {Boolean} checked
         */
        function registerCategoryProduct(grid, element, checked)
        {
            if (checked) {
                var $row = $(element).closest("tr");
                categoryProducts.set(
                    element.value,
                    [
                        $row.find("input[name=position]").val(),
                        $row.find('input[name=description]').val()
                    ]
                );
            } else {
                categoryProducts.unset(element.value);
            }
            $('#itdelight_in_category_products').val(Object.toJSON(categoryProducts));
            grid.reloadParams = {'selected_products[]': categoryProducts.keys()};
        }


        /**
         * Click on product row
         *
         * @param {Object} grid
         * @param {String} event
         */
        function categoryProductRowClick(grid, event) {
            var trElement = Event.findElement(event, 'tr'),
                isInput = Event.element(event).tagName === 'INPUT',
                checked = false,
                checkbox = null;

            if (trElement) {
                checkbox = Element.getElementsBySelector(trElement, 'input');

                if (checkbox[0]) {
                    checked = isInput ? checkbox[0].checked : !checkbox[0].checked;
                    gridJsObject.setCheckboxChecked(checkbox[0], checked);
                }
            }
        }

        /**
         * Change product data
         *
         * @param {String} event
         */
        function onChange(event)
        {
            var element = Event.element(event);
            var $row = $(element).closest("tr");
            var $checkbox = $row.find("input[type=checkbox]");

            if (!$checkbox.is(":checked")) {
                return;
            }
            categoryProducts.set(
                $checkbox.val(),
                [
                    $row.find("input[name=position]").val(),
                    $row.find('input[name=description]').val()
                ]
            );
            $categoryProducts.val(Object.toJSON(categoryProducts));
        }

        /**
         * Initialize category product row
         *
         * @param {Object} grid
         * @param {String} row
         */
        function categoryProductRowInit(grid, row)
        {
            var
                $position = $(row).find('input[name=position]'),
                $description = $(row).find('input[name=description]');

            $position.on("keyup", onChange);
            $description.on("keyup", onChange);
        }

        gridJsObject.rowClickCallback = categoryProductRowClick;
        gridJsObject.initRowCallback = categoryProductRowInit;
        gridJsObject.checkboxCheckCallback = registerCategoryProduct;

        if (gridJsObject.rows) {
            gridJsObject.rows.each(function (row) {
                categoryProductRowInit(gridJsObject, row);
            });
        }
    };
});
