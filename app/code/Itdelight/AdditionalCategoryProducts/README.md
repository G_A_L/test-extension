# Itdelight Additional Category Products

Module allow to add additional category products

## How to use

- Go to admin => Products => Categories
- Choose category
- Expand "Don't forget products"
- Add products to show on checkout cart

## Dependencies
The **Itdelight_AdditionalCategoryProducts** is dependent on the following modules:

- Magento_Catalog
