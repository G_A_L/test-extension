<?php

namespace Itdelight\AdditionalCategoryProducts\Helper;

use Itdelight\AdditionalCategoryProducts\Model\ResourceModel\Catalog\Category\Product;

/**
 * Data helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Product factrory
     *
     * @var \Itdelight\AdditionalCategoryProducts\Model\Catalog\Category\ProductFactory
     */
    protected $productFactory;

    /**
     * Session
     *
     * @var \Magento\Checkout\Model\Session
     */
    protected $session;

    /**
     * Price currency formatter
     *
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * Product collection factory
     *
     * @var Product\CollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Itdelight\AdditionalCategoryProducts\Model\Catalog\Category\ProductFactory $productFactory
     * @param Product\CollectionFactory $productCollectionFactory
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Itdelight\AdditionalCategoryProducts\Model\Catalog\Category\ProductFactory $productFactory,
        Product\CollectionFactory $productCollectionFactory,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
    ) {
        parent::__construct($context);
        $this->productFactory = $productFactory;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->priceCurrency = $priceCurrency;
    }

    /**
     * Update additional category products
     *
     * @param int $categoryId
     * @param array $products
     * @return void
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function updateCategoryProducts($categoryId, $products)
    {
        /**
         * @var Product\Collection $collection
         */
        $collection = $this->productCollectionFactory->create();
        $collection->addFieldToFilter('category_id', $categoryId);

        /** @var Product $item */
        foreach ($collection as $item) {
            if (empty($products)) {
                $item->delete();
            } else {
                if (isset($products[$item->getProductId()])) {
                    list($position, $description) = $products[$item->getProductId()];
                    $item->setPosition($position);
                    $item->setDescription($description);
                    $item->save();
                    unset($products[$item->getProductId()]);
                } else {
                    $item->delete();
                }
            }
        }

        if (!empty($products)) {
            /** @var Product $product */
            foreach ($products as $productId => $data) {
                list($position, $description) = $data;
                $product = $this->productFactory->create();
                $product->setCategoryId($categoryId);
                $product->setProductId($productId);
                $product->setPosition($position);
                $product->setDescription($description);
                $product->setType(0);
                $product->save();
            }
        }
    }

    /**
     * Retrieve formatted price
     *
     * @param float $price
     * @return string
     */
    public function formatPrice($price)
    {
        return $this->priceCurrency->format(
            $price,
            true,
            \Magento\Framework\Pricing\PriceCurrencyInterface::DEFAULT_PRECISION
        );
    }
}
