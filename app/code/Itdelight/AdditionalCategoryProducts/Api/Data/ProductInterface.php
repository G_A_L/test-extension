<?php

namespace Itdelight\AdditionalCategoryProducts\Api\Data;

/**
 * Interface ProductInterface
 *
 * @package Itdelight\AdditionalCategoryProducts\Api\Data
 */
interface ProductInterface
{
    /**
     * Additional category products data.
     */
    const CATEGORY_PRODUCT_ID = 'category_product_id';
    const CATEGORY_ID = 'category_id';
    const PRODUCT_ID = 'product_id';
    const POSITION = 'position';
    const DESCRIPTION = 'description';
    const TYPE = 'type';

    /**
     * @return mixed
     */
    public function getCategoryProductId();

    /**
     * @return mixed
     */
    public function getCategoryId();

    /**
     * @return mixed
     */
    public function getProductId();

    /**
     * @return mixed
     */
    public function getPosition();

    /**
     * @return mixed
     */
    public function getDescription();

    /**
     * @return mixed
     */
    public function getType();

    /**
     * @param $categoryProductId
     * @return mixed
     */
    public function setCategoryProductId($categoryProductId);

    /**
     * @param $categoryId
     * @return mixed
     */
    public function setCategoryId($categoryId);

    /**
     * @param $productId
     * @return mixed
     */
    public function setProductId($productId);

    /**
     * @param $position
     * @return mixed
     */
    public function setPosition($position);

    /**
     * @param $description
     * @return mixed
     */
    public function setDescription($description);

    /**
     * @param $type
     * @return mixed
     */
    public function setType($type);
}
