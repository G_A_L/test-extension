<?php

namespace Itdelight\AdditionalCategoryProducts\Block\Product;

/**
 * Class Items
 * Additional category products block
 */
class Items extends \Magento\Catalog\Block\Product\AbstractProduct
{
    /**
     * Helper
     *
     * @var \Itdelight\AdditionalCategoryProducts\Helper\Data
     */
    protected $helper;

    /**
     * Session
     *
     * @var \Magento\Checkout\Model\Session
     */
    protected $session;

    /**
     * Product collection factrory
     *
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * Image builder
     *
     * @var \Magento\Catalog\Block\Product\ImageBuilder
     */
    protected $imageBuilder;

    /**
     * Items constructor.
     *
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Itdelight\AdditionalCategoryProducts\Helper\Data $helper
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory
     * @param \Magento\Checkout\Model\Session $session
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Itdelight\AdditionalCategoryProducts\Helper\Data $helper,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\Checkout\Model\Session $session,
        array $data = []
    ) {
        $this->helper = $helper;
        $this->imageBuilder = $context->getImageBuilder();
        $this->session = $session;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * Retrieve cart items
     *
     * @return \Magento\Quote\Model\Quote\Item[]
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCartItems()
    {
        return $this->session->getQuote()->getAllVisibleItems();
    }

    /**
     * Retrieve collection of assigned additional products
     *
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getAssignedProductsByCartItems()
    {
        $categoryIds = [];
        $productsIds = [];
        foreach ($this->getCartItems() as $cartItem) {
            $product = $cartItem->getProduct();
            $ids = (array)$product->getCategoryIds();
            $productsIds[] = $product->getId();
            $categoryIds = array_merge($categoryIds, $ids);
        }

        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
        $collection = $this->collectionFactory->create();
        $collection->joinTable(
            ['cp' => \Itdelight\AdditionalCategoryProducts\Model\ResourceModel\Catalog\Category\Product::TABLE],
            'product_id=entity_id',
            ['cp.position', 'cp.description'],
            ['category_id' => ['in' => $categoryIds]]
        );
        $collection->addFieldToSelect(['name', 'price', 'small_image']);
        $collection->addFieldToFilter('entity_id', ['nin' => $productsIds]);
        $collection->setOrder('cp.position', 'asc');
        $collection->groupByAttribute('entity_id');

        return $collection;
    }

    /**
     * Retrieve data helper instance
     *
     * @return \Itdelight\AdditionalCategoryProducts\Helper\Data
     */
    public function getHelper()
    {
        return $this->helper;
    }

    /**
     * Retrieve formatted price
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getPrice($product)
    {
        return $this->helper->formatPrice($product->getData('price'));
    }
}
