<?php

namespace Itdelight\AdditionalCategoryProducts\Block\Adminhtml\Category\Tab;

use Itdelight\AdditionalCategoryProducts\Model\ResourceModel\Catalog\Category;
use Magento\Backend\Block\Widget;
use Magento\Framework\View\Element\BlockInterface;

/**
 * Class Product
 * Additional category products grid tab
 */
class Product extends Widget\Grid\Extended implements BlockInterface
{
    /**
     * Product collection factory
     *
     * @var Category\Product\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * Product factory
     *
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * Registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * Product constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param Category\Product\CollectionFactory $collectionFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\Registry $coreRegistry,
        Category\Product\CollectionFactory $collectionFactory,
        array $data = []
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->productFactory = $productFactory;
        $this->coreRegistry = $coreRegistry;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * {@inheritdoc}
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'in_category',
            [
                'type' => 'checkbox',
                'name' => 'in_category',
                'values' => $this->getSelectedProducts(),
                'index' => 'entity_id',
                'header_css_class' => 'col-select col-massaction',
                'column_css_class' => 'col-select col-massaction'
            ]
        );
        $this->addColumn(
            'entity_id',
            [
                'header' => __('ID'),
                'index' => 'entity_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );
        $this->addColumn('name', ['header' => __('Name'), 'index' => 'name']);
        $this->addColumn('sku', ['header' => __('SKU'), 'index' => 'sku']);
        $this->addColumn(
            'position',
            [
                'header' => __('Position'),
                'type' => 'number',
                'index' => 'position',
                'editable' => true
            ]
        );
        $this->addColumn(
            'description',
            [
                'header' => __('Description'),
                'type' => 'string',
                'index' => 'description',
                'sortable' => false,
                'editable' => true
            ]
        );

        return parent::_prepareColumns();
    }

    /**
     * {@inheritdoc}
     */
    public function _construct()
    {
        parent::_construct();
        $this->setId('itdelight_catalog_category_products');
        $this->setDefaultSort(Category\Product::PRIMARY_KEY);
        $this->setUseAjax(true);
    }

    /**
     * Retrieve grid URL
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('itdelight_additional_category_products/*/grid', ['_current' => true]);
    }

    /**
     * {@inheritdoc}
     */
    protected function _prepareCollection()
    {
        if ($this->getCategory()->getId()) {
            $this->setDefaultFilter(['in_category' => 1]);
        }
        $collection = $this->productFactory->create()->getCollection()->addAttributeToSelect(
            'name'
        )->addAttributeToSelect(
            'sku'
        )->joinTable(
            ['info' => Category\Product::TABLE],
            'product_id=entity_id',
            ['position', 'description'],
            'category_id=' . (int)$this->getRequest()->getParam('id', 0),
            'left'
        );
        $storeId = (int)$this->getRequest()->getParam('store', 0);
        if ($storeId > 0) {
            $collection->addStoreFilter($storeId);
        }
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * {@inheritdoc}
     */
    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in category flag
        if ($column->getId() == 'in_category') {
            $productIds = $this->getSelectedProducts();
            if (empty($productIds)) {
                $productIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', ['in' => $productIds]);
            } elseif (!empty($productIds)) {
                $this->getCollection()->addFieldToFilter('entity_id', ['nin' => $productIds]);
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    /**
     * Retrieve Category instance from core registry
     *
     * @return \Magento\Catalog\Model\Category|null
     */
    public function getCategory()
    {
        return $this->coreRegistry->registry('category');
    }

    /**
     * Retrieve selected additional products collection by category ID
     *
     * @param int $categoryId
     * @return Category\Product\Collection
     */
    public function getSelectedCollection($categoryId)
    {
        /** @var Category\Product\Collection $collection */
        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter(
            'type',
            \Itdelight\AdditionalCategoryProducts\Model\Catalog\Category\Product::TYPE_DEFAULT
        )->addFieldToFilter('category_id', $categoryId);
        return $collection;
    }

    /**
     * Retrieve array of IDs of selected additional products
     *
     * @return array
     */
    public function getSelectedProducts()
    {
        $products = $this->getRequest()->getPost('selected_products');
        if ($products === null) {
            $products = [];
            /** @var Category $item */
            foreach ($this->getSelectedCollection($this->getCategory()->getId()) as $item) {
                $products[] = $item->getProductId();
            }
        }
        return $products;
    }
}
