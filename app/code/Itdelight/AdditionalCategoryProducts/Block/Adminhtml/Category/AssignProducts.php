<?php /** @noinspection ALL */

/** @noinspection PhpFullyQualifiedNameUsageInspection */

namespace Itdelight\AdditionalCategoryProducts\Block\Adminhtml\Category;

use Itdelight\AdditionalCategoryProducts\Model\Catalog\Category\Product;

/**
 * Class AssignProducts
 * Additional category assigned product block
 */
class AssignProducts extends \Magento\Catalog\Block\Adminhtml\Category\AssignProducts
{

    protected $_template = 'Itdelight_AdditionalCategoryProducts::catalog/category/edit/assign_products.phtml';

    /**
     * Retrieve block grid
     *
     * @return \Itdelight\AdditionalCategoryProducts\Block\Adminhtml\Category\Tab\Product
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getBlockGrid()
    {
        if (null === $this->blockGrid) {
            $this->blockGrid = $this->getLayout()->createBlock(
                \Itdelight\AdditionalCategoryProducts\Block\Adminhtml\Category\Tab\Product::class,
                'itdelight.category.product.grid'
            );
        }
        return $this->blockGrid;
    }

    /**
     * Retrieve array of selected products in JSON
     *
     * @return string
     * @example {"47":["10", "Description"], "46":["20","Description"]};
     */
    public function getProductsJson()
    {
        $collection = $this->getBlockGrid()->getSelectedCollection($this->getCategory()->getId());
        $products = [];
        /** @var Product $item */
        foreach ($collection as $item) {
            $products[$item->getProductId()] = [$item->getPosition(), $item->getDescription()];
        }
        return $products ? $this->jsonEncoder->encode($products) : '{}';
    }
}
