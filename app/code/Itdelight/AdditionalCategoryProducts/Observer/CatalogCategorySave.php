<?php

namespace Itdelight\AdditionalCategoryProducts\Observer;

use Itdelight\AdditionalCategoryProducts\Helper\Data;
use Magento\Catalog\Model\Category;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class CatalogCategorySave
 * Observer for `catalog_category_prepare_save` event
 */
class CatalogCategorySave implements ObserverInterface
{
    /**
     * Helper
     *
     * @var Data
     */
    protected $helper;

    /**
     * Constructor.
     *
     * @param Data $helper
     */
    public function __construct(Data $helper)
    {
        $this->helper = $helper;
    }

    /**
     * Update additional category products
     *
     * {@inheritdoc}
     */
    public function execute(Observer $observer)
    {
        /** @var Category $category */
        $category = $observer->getCategory();
        $products = $category->getData('itdelight_category_products');
        $products = empty($products) ? [] : json_decode($products, true);
        $this->helper->updateCategoryProducts($category->getId(), $products);
    }
}
