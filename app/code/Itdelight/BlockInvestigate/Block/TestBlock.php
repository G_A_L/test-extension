<?php

namespace Itdelight\BlockInvestigate\Block;

class TestBlock extends \Magento\Framework\View\Element\Template implements
    \Magento\Framework\DataObject\IdentityInterface
{
    public function getHelloMessage()
    {
        $hello = 'This is test message';

        return $hello;
    }

    public function getIdentities()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cacheManager = $objectManager->get(\Magento\Framework\App\CacheInterface::class);
        $cacheManager->clean('block_investigate' . '_' . 'test_block');
        return ['block_investigate' . '_' . 'test_block'];
    }
}
