<?php
/**
 * @author IT Delight
 * @copyright Copyright (c) 2018 Convert (http://itdelight.com/)
 */

namespace Itdelight\UiAdmin\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    /**
     *
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

//        $table = $installer->getConnection()->newTable(
//            $installer->getTable('customer_data')
//        )->addColumn(
//            'data_id',
//            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
//            null,
//            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
//            'Data Id'
//        )->addColumn(
//            'customer_id',
//            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
//            null,
//            ['unsigned' => true, 'nullable' => false, 'default' => '0'],
//            'Customer Id'
//        )->addColumn(
//            'title',
//            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
//            255,
//            ['nullable' => false],
//            'Post Title'
//        )->addColumn(
//            'content',
//            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
//            '2M',
//            [],
//            'Post Content'
//        )->addColumn(
//            'created_at',
//            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
//            null,
//            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
//            'Created At'
//        )->addColumn(
//            'updated_at',
//            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
//            null,
//            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
//            'Updated At'
//        )->addColumn(
//            'image',
//            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
//            256,
//            [],
//            'Image'
//        )->addColumn(
//            'is_active',
//            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
//            null,
//            ['nullable' => false, 'default' => '1'],
//            'Is Active'
//        )->addForeignKey(
//            $installer->getFkName(
//                'customer_data',
//                'customer_id',
//                'customer_entity',
//                'entity_id'
//            ),
//            'customer_id',
//            $installer->getTable('customer_entity'),
//            'entity_id',
//            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
//        )->setComment(
//            'Customer Data'
//        );
//        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}
