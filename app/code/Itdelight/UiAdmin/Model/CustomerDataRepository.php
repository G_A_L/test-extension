<?php

namespace Itdelight\UiAdmin\Model;

use Exception;
use Itdelight\UiAdmin\Api\CustomerDataRepositoryInterface;
use Itdelight\UiAdmin\Api\Data\CustomerDataInterface;
use Itdelight\UiAdmin\Api\Data\CustomerDataSearchResultInterface;
use Itdelight\UiAdmin\Api\Data\CustomerDataSearchResultInterfaceFactory;
use Itdelight\UiAdmin\Model\CustomerDataFactory;
use Itdelight\UiAdmin\Model\ResourceModel\CustomerData as CustomerDataResource;
use Itdelight\UiAdmin\Model\ResourceModel\CustomerData\Collection;
use Itdelight\UiAdmin\Model\ResourceModel\CustomerData\CollectionFactory as CustomerDataCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class CustomerDataRepository
 *
 * @package Itdelight\UiAdmin\Model
 */
class CustomerDataRepository implements CustomerDataRepositoryInterface
{
    /**
     * @var CustomerDataResource
     */
    protected $resource;

    /**
     * @var CustomerDataFactory
     */
    protected $customerDataFactory;

    /**
     * @var CustomerDataCollectionFactory
     */
    protected $customerDataCollectionFactory;

    /**
     * @var CustomerDataSearchResultInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @param CustomerDataResource $resource
     * @param CustomerDataFactory $customerDataFactory
     * @param CustomerDataCollectionFactory $customerDataCollectionFactory
     * @param CustomerDataSearchResultInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        CustomerDataResource $resource,
        CustomerDataFactory $customerDataFactory,
        CustomerDataCollectionFactory $customerDataCollectionFactory,
        CustomerDataSearchResultInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->customerDataFactory = $customerDataFactory;
        $this->customerDataCollectionFactory = $customerDataCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @param CustomerDataInterface $customerData
     * @return CustomerDataInterface
     * @throws CouldNotSaveException
     */
    public function save(CustomerDataInterface $customerData)
    {
        try {
            $this->resource->save($customerData);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $customerData;
    }

    /**
     * @param int $customerDataId
     * @return CustomerDataInterface
     * @throws NoSuchEntityException
     */
    public function getById($customerDataId)
    {
        $customerData = $this->customerDataFactory->create();
        $this->resource->load($customerData, $customerDataId);
        if (!$customerData->getId()) {
            throw new NoSuchEntityException(__('Customer Data with id "%1" does not exist.', $customerDataId));
        }
        return $customerData;
    }

    /**
     * Test
     *
     * @param SearchCriteriaInterface $criteria
     * @return CustomerDataSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $criteria)
    {
        /** @var Collection $collection */
        $collection = $this->customerDataCollectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        /** @var CustomerDataSearchResultInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }

    /**
     * @param CustomerDataInterface $customerData
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(CustomerDataInterface $customerData)
    {
        try {
            $this->resource->delete($customerData);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * @param int $customerDataId
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($customerDataId)
    {
        return $this->delete($this->getById($customerDataId));
    }
}
