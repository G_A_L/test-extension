<?php

namespace Itdelight\UiAdmin\Model\ResourceModel\CustomerData;

use Itdelight\UiAdmin\Model\ResourceModel\CustomerData;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'customer_event_id';

    protected function _construct()
    {
        $this->_init(
            \Itdelight\UiAdmin\Model\CustomerData::class,
            CustomerData::class
        );
    }
}
