<?php

namespace Itdelight\UiAdmin\Model;

use Itdelight\UiAdmin\Api\Data\CustomerDataExtensionInterface;
use Itdelight\UiAdmin\Api\Data\CustomerDataInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class CustomerData
 *
 * @package Itdelight\UiAdmin\Model
 */
class CustomerData extends AbstractModel implements
    CustomerDataInterface,
    IdentityInterface
{
    /**
     *
     */
    const CACHE_TAG = 'itdelight_uiadmin_customerdata';
    /**
     * @var string
     */
    protected $_cacheTag = 'itdelight_uiadmin_customerdata';
    /**
     * @var string
     */
    protected $_eventPrefix = 'itdelight_uiadmin_customerdata';

    /**
     *
     */
    protected function _construct()
    {
        $this->_init(ResourceModel\CustomerData::class);
    }

    /**
     * @return array|string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return mixed|void
     */
    public function getDataId()
    {
        $this->getData(self::DATA_ID);
    }

    /**
     * @return mixed|void
     */
    public function getCustomerId()
    {
        $this->getData(self::CUSTOMER_ID);
    }

    /**
     * @return mixed|void
     */
    public function getTitle()
    {
        $this->getData(self::TITLE);
    }

    /**
     * @return mixed|void
     */
    public function getContent()
    {
        $this->getData(self::CONTENT);
    }

    /**
     * @return mixed|void
     */
    public function getCreatedAt()
    {
        $this->getData(self::CREATED_AT);
    }

    /**
     * @return mixed|void
     */
    public function getUpdatedAt()
    {
        $this->getData(self::UPDATED_AT);
    }

    /**
     * @return mixed|void
     */
    public function getImage()
    {
        $this->getData(self::IMAGE);
    }

    /**
     * @return mixed|void
     */
    public function getIsActive()
    {
        $this->getData(self::IS_ACTIVE);
    }

    /**
     * @return CustomerDataExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * @param $dataId
     * @return mixed|void
     */
    public function setDataId($dataId)
    {
        $this->setData(self::DATA_ID, $dataId);
    }

    /**
     * @param $customerId
     * @return mixed|void
     */
    public function setCustomerId($customerId)
    {
        $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * @param $title
     * @return mixed|void
     */
    public function setTitle($title)
    {
        $this->setData(self::TITLE, $title);
    }

    /**
     * @param $content
     * @return mixed|void
     */
    public function setContent($content)
    {
        $this->setData(self::CONTENT, $content);
    }

    /**
     * @param $createdAt
     * @return mixed|void
     */
    public function setCreatedAt($createdAt)
    {
        $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * @param $updatedAt
     * @return mixed|void
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->setData(self::UPDATED_AT, $updatedAt);
    }

    /**
     * @param $image
     * @return mixed|void
     */
    public function setImage($image)
    {
        $this->setData(self::IMAGE, $image);
    }

    /**
     * @param $isActive
     * @return mixed|void
     */
    public function setIsActive($isActive)
    {
        $this->setData(self::IS_ACTIVE, $isActive);
    }

    /**
     * @param CustomerDataExtensionInterface $extensionAttributes
     * @return CustomerDataInterface
     */
    public function setExtensionAttributes(CustomerDataExtensionInterface $extensionAttributes)
    {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}
