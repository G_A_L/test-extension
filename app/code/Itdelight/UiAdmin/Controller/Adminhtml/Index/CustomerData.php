<?php

namespace Itdelight\UiAdmin\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;

abstract class CustomerData extends Action
{
    const ADMIN_RESOURCE = 'Itdelight_UiAdmin::data';

    /**
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * @param Context $context
     * @param Registry $coreRegistry
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    public function initPage($resultPage)
    {
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE)
            ->addBreadcrumb(__('Customer Data'), __('Customer Data'))
            ->addBreadcrumb(__('Customer Data Test'), __('Customer Data Test'));
        return $resultPage;
    }
}
