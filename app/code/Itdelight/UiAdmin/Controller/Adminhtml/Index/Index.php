<?php

namespace Itdelight\UiAdmin\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    const ADMIN_RESOURCE = 'Itdelight_UiAdmin::data';

    protected $resultFactory;

    public function __construct(
        Action\Context $context,
        PageFactory $resultFactory
    ) {
        parent::__construct($context);
        $this->resultFactory = $resultFactory;
    }

    public function execute()
    {
        $result = $this->resultFactory->create();

        return $result;
    }
}
