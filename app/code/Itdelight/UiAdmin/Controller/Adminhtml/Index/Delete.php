<?php

namespace Itdelight\UiAdmin\Controller\Adminhtml\Index;

use Exception;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;

class Delete extends CustomerData
{
    /**
     * @var CustomerData
     */
    protected $model;

    public function __construct(
        Context $context,
        Registry $coreRegistry,
        \Itdelight\UiAdmin\Model\CustomerData $customerData
    ) {
        $this->model = $customerData;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Delete action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('data_id');
        if ($id) {
            try {
                // init model and delete
                $this->model->load($id);
                $this->model->delete();
                // display success message
                $this->messageManager->addSuccessMessage(__('You deleted the Complaint.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['data_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a Customer Data to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
