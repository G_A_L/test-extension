<?php

namespace Itdelight\UiAdmin\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Edit
 *
 * @package Itdelight\UiAdmin\Controller\Adminhtml\Index
 */
class Edit extends CustomerData implements HttpGetActionInterface
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Itdelight\UiAdmin\Model\CustomerData
     */
    protected $customerData;

    /**
     * Edit constructor.
     *
     * @param Context $context
     * @param Registry $coreRegistry
     * @param PageFactory $resultPageFactory
     * @param \Itdelight\UiAdmin\Model\CustomerData $customerData
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        \Itdelight\UiAdmin\Model\CustomerData $customerData
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->customerData = $customerData;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit CMS block
     *
     * @return ResultInterface
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('data_id');
        $model = $this->customerData;

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This customer data no longer exists.'));
                /** @var Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        $this->_coreRegistry->register('customer_data', $model);

        // 5. Build edit form
        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Customer Data') : __('New  Customer Data'),
            $id ? __('Edit  Customer Data') : __('New  Customer Data')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Customer Data'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? $model->getData('title') : __('New Block'));
        return $resultPage;
    }
}
