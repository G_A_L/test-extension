<?php
/**
 * @author IT Delight
 * @copyright Copyright (c) 2019 Convert (http://itdelight.com/)
 */

namespace Itdelight\UiAdmin\Ui\DataProvider\Form;

use Itdelight\UiAdmin\Model\ResourceModel\CustomerData\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\Modifier\PoolInterface;
use Magento\Ui\DataProvider\ModifierPoolDataProvider;

/**
 * Class DataProvider
 */
class DataProvider extends ModifierPoolDataProvider
{
    /**
     * @var CollectionFactory
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $customerDataCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     * @param PoolInterface|null $pool
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $customerDataCollectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = [],
        PoolInterface $pool = null
    ) {
        $this->collection = $customerDataCollectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data, $pool);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $customerData) {
            $this->loadedData[$customerData->getData('data_id')] = $customerData->getData();
        }

        $data = $this->dataPersistor->get('customer_data');
        if (!empty($data)) {
            $customerData = $this->collection->getNewEmptyItem();
            $customerData->setData($data);
            $this->loadedData[$customerData->getData('data_id')] = $customerData->getData();
            $this->dataPersistor->clear('customer_data');
        }

        return $this->loadedData;
    }
}
