<?php

namespace Itdelight\UiAdmin\Ui\DataProvider\Listing;

use Itdelight\UiAdmin\Model\ResourceModel\CustomerData\CollectionFactory;
use Magento\Ui\DataProvider\AbstractDataProvider;

class CustomerDataDataProvider extends AbstractDataProvider
{
    /**
     * Customer Data collection
     *
     * @var CollectionFactory
     */
    protected $collection;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (!$this->getCollection()->isLoaded()) {
            $this->getCollection()->load();
        }
        $items = $this->getCollection()->toArray();

        return $items;
    }
}
