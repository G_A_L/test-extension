<?php

namespace Itdelight\UiAdmin\Api;

use Itdelight\UiAdmin\Api\Data\CustomerDataInterface;
use Itdelight\UiAdmin\Api\Data\CustomerDataSearchResultInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Interface CustomerDataRepositoryInterface
 * @package Itdelight\UiAdmin\Api
 */
interface CustomerDataRepositoryInterface
{
    /**
     * @param Data\CustomerDataInterface $customerData
     * @return CustomerDataInterface
     * @throws LocalizedException
     */
    public function save(CustomerDataInterface $customerData);

    /**
     * @param int $customerDataId
     * @return CustomerDataInterface
     * @throws LocalizedException
     */
    public function getById($customerDataId);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return CustomerDataSearchResultInterface
     * @throws LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * @param Data\CustomerDataInterface $customerData
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(CustomerDataInterface $customerData);

    /**
     * @param int $customerDataId
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($customerDataId);
}
