<?php

namespace Itdelight\UiAdmin\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * Interface CustomerDataInterface
 *
 * @package Itdelight\UiAdmin\Api\Data
 */
interface CustomerDataInterface extends ExtensibleDataInterface
{
    /**
     * Data constants
     */
    const DATA_ID = 'data_id';
    const CUSTOMER_ID = 'customer_id';
    const TITLE = 'title';
    const CONTENT = 'content';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    const IMAGE = 'image';
    const IS_ACTIVE = 'is_active';

    /**
     * @return mixed
     */
    public function getDataId();

    /**
     * @return mixed
     */
    public function getCustomerId();

    /**
     * @return mixed
     */
    public function getTitle();

    /**
     * @return mixed
     */
    public function getContent();

    /**
     * @return mixed
     */
    public function getCreatedAt();

    /**
     * @return mixed
     */
    public function getUpdatedAt();

    /**
     * @return mixed
     */
    public function getImage();

    /**
     * @return mixed
     */
    public function getIsActive();

    /**
     * @return CustomerDataExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * @param $dataId
     * @return mixed
     */
    public function setDataId($dataId);

    /**
     * @param $customerId
     * @return mixed
     */
    public function setCustomerId($customerId);

    /**
     * @param $title
     * @return mixed
     */
    public function setTitle($title);

    /**
     * @param $content
     * @return mixed
     */
    public function setContent($content);

    /**
     * @param $createdAt
     * @return mixed
     */
    public function setCreatedAt($createdAt);

    /**
     * @param $updatedAt
     * @return mixed
     */
    public function setUpdatedAt($updatedAt);

    /**
     * @param $image
     * @return mixed
     */
    public function setImage($image);

    /**
     * @param $isActive
     * @return mixed
     */
    public function setIsActive($isActive);

    /**
     * @param CustomerDataExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(CustomerDataExtensionInterface $extensionAttributes);
}
