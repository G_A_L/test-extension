<?php

namespace Itdelight\UiAdmin\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface CustomerDataSearchResultInterface
 * @package Itdelight\UiAdmin\Api\Data
 */
interface CustomerDataSearchResultInterface extends SearchResultsInterface
{
    /**
     * @return CustomerDataInterface[]
     */
    public function getItems();

    /**
     * @param CustomerDataInterface[] $items
     * @return SearchResultsInterface
     */
    public function setItems(array $items = null);
}
