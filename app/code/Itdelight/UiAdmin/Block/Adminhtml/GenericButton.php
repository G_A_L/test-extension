<?php
/**
 * @author IT Delight
 * @copyright Copyright (c) 2018 Convert (http://itdelight.com/)
 */

namespace Itdelight\UiAdmin\Block\Adminhtml;

use Magento\Backend\Block\Widget\Context;

/**
 * Class GenericButton
 * @package Noveo\ReclamationOrder\Block\Adminhtml\Complaint\Edit
 */
abstract class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @param Context $context
     */
    public function __construct(Context $context)
    {
        $this->context = $context;
    }

    /**
     * Return model ID
     *
     * @return int|null
     */
    public function getModelId()
    {
        return $this->context->getRequest()->getParam('data_id');
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
