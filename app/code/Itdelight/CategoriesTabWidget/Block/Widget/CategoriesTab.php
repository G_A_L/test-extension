<?php

namespace Itdelight\CategoriesTabWidget\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;

/**
 * Class CategoriesTab
 *
 * @package Itdelight\CategoriesTabWidget\Block\Widget
 */
class CategoriesTab extends Template implements BlockInterface
{
    /**
     * @var CollectionFactory
     */
    protected $categoryCollectionFactory;

    /**
     * CategoriesTab constructor.
     *
     * @param Template\Context $context
     * @param CollectionFactory $categoryCollectionFactory
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        CollectionFactory $categoryCollectionFactory,
        array $data = []
    ) {
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * Get main categories for tabs title.
     *
     * @return mixed
     */
    public function getMainCategories()
    {
        return $this->getCategoryCollection()
            ->addAttributeToFilter('entity_id', explode(',', $this->getCategoriesIds()));
    }

    /**
     * Get needed data for children categories.
     *
     * @param $category
     * @return mixed
     */
    public function getChildrenCategoryData($category)
    {
        return $this->getCategoryCollection()
            ->addAttributeToFilter('entity_id', explode(',', $category->getChildren()));
    }

    /**
     * Collection for getting categories by ids.
     *
     * @param bool $isActive
     * @param bool $level
     * @param bool $sortBy
     * @param bool $pageSize
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getCategoryCollection($isActive = true, $level = false, $sortBy = false, $pageSize = false)
    {
        $collection = $this->categoryCollectionFactory->create();
        $collection->addAttributeToSelect('name')
        ->addAttributeToSelect('url_path')
        ->addAttributeToSelect('image')
        ->addAttributeToSelect('description');

        // select only active categories
        if ($isActive) {
            $collection->addIsActiveFilter();
        }

        // select categories of certain level
        if ($level) {
            $collection->addLevelFilter($level);
        }

        // sort categories by some value
        if ($sortBy) {
            $collection->addOrderField($sortBy);
        }

        // select certain number of categories
        if ($pageSize) {
            $collection->setPageSize($pageSize);
        }

        return $collection;
    }
}
