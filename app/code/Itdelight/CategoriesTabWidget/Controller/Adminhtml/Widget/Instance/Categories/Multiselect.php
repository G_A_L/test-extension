<?php

namespace Itdelight\CategoriesTabWidget\Controller\Adminhtml\Widget\Instance\Categories;

use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Math\Random;
use Magento\Framework\View\Layout;
use Magento\Widget\Block\Adminhtml\Widget\Catalog\Category\Chooser;

/**
 * Class Multiselect
 *
 * @package Itdelight\CategoriesTabWidget\Controller\Adminhtml\Widget\Instance\Categories
 */
class Multiselect extends Action
{
    const ADMIN_RESOURCE = 'Itdelight_CategoriesTabWidget::categories_multiselect';

    /**
     * @var Layout
     */
    protected $layout;

    /**
     * @var Random
     */
    protected $mathRandom;

    /**
     * Multiselect constructor.
     *
     * @param Action\Context $context
     * @param Random $mathRandom
     * @param Layout $layout
     */
    public function __construct(
        Action\Context $context,
        Random $mathRandom,
        Layout $layout
    ) {
        $this->layout = $layout;
        $this->mathRandom = $mathRandom;
        parent::__construct($context);
    }

    /**
     * Categories chooser Action (Ajax request)
     *
     * @return \Magento\Framework\Controller\Result\Raw
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $selected = $this->getRequest()->getParam('selected', '');

        /** @var \Magento\Widget\Block\Adminhtml\Widget\Catalog\Category\Chooser $chooser */
        $chooser = $this->layout->createBlock(Chooser::class)
            ->setTemplate('Itdelight_CategoriesTabWidget::catalog/category/widget/multi-select-tree.phtml')
            ->setUseMassaction(true)
            ->setId($this->mathRandom->getUniqueHash('categories'))
            ->setSelectedCategories(explode(',', $selected));

        /** @var \Magento\Framework\Controller\Result\Raw $resultRaw */
        $resultRaw = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        return $resultRaw->setContents($chooser->toHtml());
    }
}
